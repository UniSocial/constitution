# Constitution

UniSocial.Net has 3 types of "asset backed" cryptocurrency, tied to different classes of investments:
- UniCoin  = "Safe" and fixed income investments: treasuries, bonds, debt-backed & MBSs. Also metals & commodities, like Gold & Silver & Platinum
- Gemini = Risky financial assets and Equities!
- DreamCoin = Hardware!

# Changing your Portfolio:
UniBank lets you convert your holdings between all 3 at 0 Zero bid-ask spreads and no trading costs!

# Converting to other currencies:
However exchanging one for a coin outside our system has some but very low trading fees!


# What is DreamCoin?
DreamCoin is Commodity market for computing resources, and a Miner's currency!

You not only get paid in DreamCoin for mining, You also can install cloud Apps that run in it! OR Invest in other miners' ventures: Thereby giving the capital in exchange for dividends later!

A DreamCoin is a share of ownership in the infrastructure that runs Uni.Network & BlackCubeAI.

All of our software is designed to run on dreamcoin "Hardware backed security" commodities!

# What are DreamAds, & Dream.Net?
DreamAds are an alternative Advertising Network that runs across Uni.Network & UniSocial.Net! We take the best lessons from the industry & cognitive psychology and create a truly fair and effective auction market for online advertising. There are also incentives put in place to Reduce advertiser's abuse of Consumers, and Fraud!

Dream.Net is an alternative Affiliate Marketing network as well!